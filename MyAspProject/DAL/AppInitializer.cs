﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using MyAspProject.Models;

namespace MyAspProject.Context
{
	public class AppInitializer : DropCreateDatabaseAlways<AppContext>
	{
		protected override void Seed(AppContext context) 
		{
			List<Actor> actors = new List<Actor> {
				new Actor("Mark Hamill", new DateTime(1951, 09, 25)),
				new Actor("Harrison Ford", new DateTime(1942, 07, 13)),
				new Actor("Carrie Fisher", new DateTime(1956, 10, 21)),
				new Actor("David Prowse", new DateTime(1935, 01, 01)),
				new Actor("Keanu Reeves", new DateTime(1964, 09, 02)),
				new Actor("Laurence Fishburne", new DateTime(1961, 07, 30)),
				new Actor("Carrie-Anne Moss ", new DateTime(1967, 08, 21)),
				new Actor("Natalie Portman", new DateTime(1981, 06, 09))
			};

			List<Film> films = new List<Film> {
				new Film("Star Wars", new DateTime(1977, 05, 25)),
				new Film("The Matrix", new DateTime(1999, 03, 24))
			};

			actors[0].Films.Add(films[0]);
			actors[4].Films.Add(films[1]);

			actors.ForEach (actor => context.Actors.Add (actor));
			films.ForEach (film => context.Films.Add (film));
			context.SaveChanges ();
		}
	}
}

